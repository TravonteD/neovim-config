(require-macros :fnl.macros)

(set vim.g.mapleader " ")
(set vim.g.maplocalleader ",")

(safe_require :plugins)
(safe_require :treesitter)
(safe_require :options)
(safe_require :highlight)
(safe_require :keys)
(safe_require :commands)
(safe_require :autocmds)
