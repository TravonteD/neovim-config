(local M {})
(fn M.register_snippets [filetype snippets]
  (each [n _ (pairs snippets)]
    (let [set_mark " <c-o>ms"
          insert_snippet (.. "<c-r>=luaeval('require(\"funcs\").snippet(\"" filetype "\", \"" n "\")')<cr>")
          return_to_mark "<esc>'s"
          replace_placeholder "/<++><cr>c4l<c-r>=luaeval('require(\"funcs\").eatchar(\" \")')<cr>"]
      (vim.cmd (.. "inoreabbr <silent> <buffer> " 
                   (table.concat [n set_mark insert_snippet return_to_mark replace_placeholder] ""))))))

(fn M.setup [opts]
  (when (and opts.snippets opts.filetype)
    (M.register_snippets opts.filetype opts.snippets)))

M
