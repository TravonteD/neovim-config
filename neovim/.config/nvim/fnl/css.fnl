(require-macros :macros)

(set vim.opt_local.tabstop 4)
(set vim.opt_local.shiftwidth 4)
