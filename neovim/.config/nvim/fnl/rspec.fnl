(require-macros :macros)

(set vim.opt_local.define "^\\s*\\(def\\|describe\\)")
(set vim.opt_local.include "^\\s*\\(require_relative\\|require\\)")

(nnoremap "<leader>r" ":RunAllSpecs<cr>" [:buffer :silent])

(command! :RunAllSpecs "lua Runspec(0)" [:buffer])
(command! :RunSingleSpec "lua RunSpec(vim.fn.line('.'))" [:buffer])

(local M {:filetype "rspec"
          :snippets {:describe. "describe '<++>' do\nend"
                     :it. "it '<++>' do\nend"
                     :context. "context '<++>' do\nend"
                     :let. "let(:<++>) { <++> }"
                     :expect. "expect(<++>).to <++>"
                     :allow. "allow(<++>).to receive(<++>)"
                     :before. "before do\n  <++>\nend"}})

(fn _G.RunSpec [line]
  (var line line)
  (if (= line 0)
    (set line "")
    (if (string.find (vim.fn.getline line) "describe")
      (set line (.. ":" (vim.fn.search "describe" "b")))
      (set line (.. ":" line))))
  (vim.cmd (.. "split term://rspec %" line)))

(mcall :config :setup M)
M
