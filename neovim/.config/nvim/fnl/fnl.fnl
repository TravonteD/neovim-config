(require-macros :macros)

(set vim.opt_local.suffixesadd ".fnl")
(set vim.opt_local.define "(\\(fn\\|macro\\|lambda\\|λ\\)\\s\\zs\\w")
(set vim.opt_local.include "(\\(require\\-macros\\|import\\-macros\\|require\\)")
(set vim.opt_local.includeexpr "substitute(v:fname,'\\.','/','g')")
(set vim.opt_local.lispwords (.. vim.o.lispwords ",collect,icollect,with-open"))
(nnoremap :K ":lua require'iron'.core.send('fennel', ('(doc '..vim.fn.expand('<cword>')..')'))<cr>" [:buffer :silent])
(inoreabbrev :lambda :λ)

(local M {:filetype "fennel"
          :snippets {:format. "string.format <++>"
                     :match. "string.match <++>"
                     :gsub. "string.gsub <++>"
                     :concat. "table.concat <++>"}})

(λ M.export-methods []
  ; Go to the bottom of the file and create a table
  (vim.cmd "normal! Go{")
  ; Grep for all of the method names in the file and read them in
  (vim.cmd "r!rg -N '\\s*\\([λ|lambda|fn] (\\S+) \\[.*'  -r ': $1' %")
  ; Close the table
  (vim.cmd "normal! Go}"))
(command! :ExportMethods "lua require('fnl')['export-methods']()" [:buffer])


(λ M.toggle-repl-client []
  "Toggles between repl clients for conjure (stdio, aniseed) and restarts the repl"
  (set vim.g.conjure#filetype#fennel 
       (if (= vim.g.conjure#filetype#fennel "conjure.client.fennel.stdio")
         "conjure.client.fennel.aniseed"
         "conjure.client.fennel.stdio"))
  (vim.cmd "ConjureFnlStop")
  (vim.cmd "ConjureFnlStart"))
(command! :ConjureFnlToggleClient "lua require('fnl')['toggle-repl-client']()" [:buffer])
(mcall :config :setup M)
M
