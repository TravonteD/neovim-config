(require-macros :macros)

(set vim.opt_local.define "^\\s*\\(def\\|private\\s*def\\|class\\|struct\\)")
(set vim.opt_local.include "^\\s*\\(require\\)")
(set vim.opt_local.path ".,src/**,lib/**/**,/lib/crystal/**")
(set vim.opt_local.suffixesadd ".cr")
(set vim.opt_local.suffixes "")
(set vim.opt_local.makeprg :crystal)
(set vim.opt_local.iskeyword (+ vim.bo.iskeyword ":"))
(set vim.opt_local.foldmethod :indent)

; TODO: Implement this nicely
; let Ifunc = {file -> file . '.cr'}
; setlocal includeexpr=Ifunc(v:fname)

(command! :Format "silent !crystal tool format %" [:buffer])
(command! :Extract "lua CrystalExtract()" [:buffer])

(xnoremap "<leader>v" ":<C-u>Extract<cr>" [:buffer])

(local M {:filetype :crystal
          :snippets {:def. "def <++>\nend"
                     :class. "class <++>\nend"
                     :module. "module <++>\nend"
                     :if. "if <++>\nend"
                     :select_ "select { |x| <++> }"
                     :Select_ "select do |<++>|\nend"
                     :reject_ "reject { |x| <++> }"
                     :Reject_ "reject do |<++>|\nend"
                     :each_ "each { |x| <++> }"
                     :Each_ "each do |<++>|\nend"
                     }})
(global CrystalExtract (mcall :funcs :make_var_extract ""))
(mcall :config :setup M)
M
