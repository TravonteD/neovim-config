(require-macros :macros)

(set vim.opt_local.foldlevel 100)
(set vim.opt_local.foldexpr "nvim_treesitter#foldexpr()")
(set vim.opt_local.foldmethod "expr")
(set vim.opt_local.makeprg "rubocop -l -f emacs %")
(set vim.opt_local.define "^\\s*def")
(set vim.opt_local.include "^\\s*\\(require_relative\\|require\\)")

(command! :RunFile ":vsp term://ruby %" [:buffer])
(command! :Repl ":vsp term://irb %" [:buffer])
(command! :OpenSpec ":vsp spec/%:t:r_spec.rb" [:buffer])
(command! :Extract ":lua RubyExtract()" [:buffer])
(nnoremap "<localleader>t" ":lua require'ruby'.run_specs()<cr>" [:buffer :silent])

(vim.cmd "command! -buffer -nargs=? -complete=custom,v:lua.RakeTasks Rake 20split term://rake <args>")
(global RakeTasks
  (fn [A L P]
    (vim.fn.system "rake -P | rg 'rake' | cut -d' ' -f2")))

(λ run_specs [] 
  (let [iron (require :iron)]
    (iron.core.focus_on :zsh)
    (iron.core.send :zsh "bundle exec rspec")))

(local M {:filetype "ruby"
          :snippets {:def. "def <++>\nend"
                     :class. "class <++>\nend"
                     :module. "module <++>\nend"
                     :if. "if <++>\nend"
                     :select_ "select { |x| <++> }"
                     :Select_ "select do |<++>|\nend"
                     :reject_ "reject { |x| <++> }"
                     :Reject_ "reject do |<++>|\nend"
                     :each_ "each { |x| <++> }"
                     :Each_ "each do |<++>|\nend"
                     :frozen. "# frozen_string_literal true"
                     }
          : run_specs})
(global RubyExtract (mcall :funcs :make_var_extract ""))
(mcall :config :setup M)
M
