(require-macros :macros)

(nnoremap "<leader>a" ":LedgerAlignBuffer<cr>:w<cr>" [:buffer :silent])
(nnoremap "<leader>b" ":Ledger balance not Equity<cr>" [:buffer])
