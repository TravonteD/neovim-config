(local M {:filetype "rust"
          :snippets {:fn. "fn <++>(<++>) -> <++> {\n}"
                     :print. "println!(<++>)"}})
(mcall :config :setup M)
M
