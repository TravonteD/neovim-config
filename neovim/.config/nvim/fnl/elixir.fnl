(require-macros :macros)

(set vim.opt_local.formatprg "mix format -")
(command! :RunFile ":vsp term://elixir %<cr>" [:buffer])
(nnoremap "<localleader>s" ":lua require'elixir'.spec()<cr>" [:buffer :silent])
(nnoremap "<localleader>t" ":lua require'elixir'.run_spec()<cr>" [:buffer :silent])

(λ current_spec_file []
  (let [current_file (vim.fn.expand "%")]
    (if (string.match current_file "test")
      current_file
      (-> current_file
          (string.gsub "lib" "test")
          (string.gsub ".ex" "_test.exs")))))

(λ spec [] 
  (let [original_file (vim.fn.expand "%")]
    (var new_file (if (original_file:find "test")
                    (-> original_file
                        (string.gsub "_test.exs" ".ex")
                        (string.gsub "test" "lib"))
                    (-> original_file
                        (string.gsub "lib" "test")
                        (string.gsub ".ex" "_test.exs"))))
    (vim.cmd (.. "edit " new_file))))

(λ run_spec [] 
  (let [iron (require :iron)
        file (current_spec_file)]
    (iron.core.focus_on :zsh)
    (iron.core.send :zsh (.. "mix test " file))))

(local M {:filetype :elixir
          :snippets {:def. "def <++> do\nend"
                     :defmodule. "defmodule <++> do\nend"
                     :if. "if <++> do\nend"
                     :pry. "require IEx; IEx.pry"}
          : spec
          : run_spec})
(mcall :config :setup M)
M
