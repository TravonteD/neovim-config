(import-macros {: with-lua} :macros)
(with-lua
  [:ts :nvim-treesitter.config]
  (let [parser_list ["fennel" "ruby" "python" "javascript" "typescript" "html" "css" "lua" "go"]]
    (ts.setup {:ensure_installed parser_list
               :highlight {:enable parser_list}
               :refactor {:highlight_definitions {:enable false}
                          :highlight_current_scope {:enable false}
                          :smart_rename {:enable true
                                         :keymaps {:smart_rename "grr"}}
                          :navigation {:enable true
                                       :keymaps {:list_definitions_toc "gO"}}}})))

