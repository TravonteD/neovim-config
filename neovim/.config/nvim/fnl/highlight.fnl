(require-macros :macros)
; Highlights
(local colors {:white :#1d2021
               :gray :#a0a1a7
               :dim-gray :#6f7579})
(highlight! :Comment {:cterm :italic 
                      :gui :italic})
(highlight! :Conceal {:ctermbg :NONE 
                      :guibg :NONE})
(highlight! :Cursor {:ctermfg :NONE :ctermbg :White :cterm :bold
                     :guifg :NONE :guibg :White :gui :bold})
(highlight! :CursorLine {:ctermbg :NONE 
                         :guibg :NONE})
(highlight! :CursorLineNr {:ctermfg :DarkRed :ctermbg :NONE 
                           :guibg :NONE})
(highlight! :DiffAdd {:ctermbg :DarkCyan :ctermfg :0 
                      :guibg :DarkCyan :guifg :Black})
(highlight! :DiffChange {:ctermbg :DarkCyan :ctermfg :0})
(highlight! :DiffDelete {:ctermbg :DarkRed :ctermfg :0 
                         :guibg :DarkRed :guifg :0})
(highlight! :DiffText {:ctermbg :DarkCyan :ctermfg :0 :cterm "undercurl,bold" 
                       :gui "undercurl,bold"})
(highlight! :Delimiter {:ctermfg :8 
                        :guifg :#6f7579})
(highlight! :EndOfBuffer {:ctermfg :Black})
(highlight! :FoldColumn {:ctermbg :NONE 
                         :guibg :NONE})
(highlight! :Folded {:cterm :italic 
                     :gui :italic})
(highlight! :Folded {:ctermbg :NONE 
                     :guibg :NONE})
(highlight! :Identifier {:ctermbg :NONE 
                         :guifg :White :guibg :NONE})
(highlight! :IncSearch {:ctermbg :NONE :cterm :undercurl :ctermfg :DarkGreen 
                        :guibg :NONE :gui :undercurl})
(highlight! :LineNr {:ctermbg :NONE 
                     :guibg :NONE})
(highlight! :MatchParen {:ctermbg :NONE :ctermfg :White :cterm :bold
                         :guibg :NONE :guifg :White :gui :bold})
(highlight! :Normal {:ctermbg :NONE 
                     :guibg :NONE
                     :guifg :#a0a1a7})
(highlight! :Pmenu {:ctermbg :Black :ctermfg :White
                    :guibg :NONE})
(highlight! :PmenuSel {:ctermbg :White :ctermfg :Black
                       :guibg :White :guifg :Black})
(highlight! :SignColumn {:ctermbg :NONE 
                         :guibg :NONE})
(highlight! :Special {:guifg :#6f7579})
(highlight! :SpellBad {:ctermbg :NONE :cterm :undercurl :ctermfg :DarkRed 
                       :guibg :NONE :gui :undercurl})
(highlight! :SpellCap {:ctermbg :NONE :cterm :undercurl :ctermfg :DarkBlue 
                       :guibg :NONE :gui :undercurl})
(highlight! :StatusLine {:ctermbg :NONE 
                         :guibg :NONE})
(highlight! :StatusLineNC {:ctermbg :NONE 
                           :guibg :NONE})
(highlight! :String {:guifg :#a0a1a7 :gui :bold})
(highlight! :TabLine {:ctermbg :NONE :cterm :NONE 
                      :guibg :NONE :gui :NONE})
(highlight! :TabLineFill {:ctermbg :NONE :cterm :NONE 
                          :guibg :NONE :gui :NONE})
(highlight! :TabLineSel {:ctermbg :NONE :cterm :NONE 
                         :guibg :NONE :gui :NONE})
(highlight! :Todo {:ctermfg :Yellow :ctermbg :NONE 
                   :guibg :NONE})
(highlight! :VertSplit {:cterm :NONE :ctermbg :NONE 
                        :gui :NONE :guibg :NONE})
(highlight! :Visual {:ctermbg :DarkGray})
(hilink! :TSVariable :Identifier)
(hilink! :Statement :Define)

;; Fennel
(hilink! :LuaSpecialValue :Function)
(hilink! :FennelSpecialForm :Function)
(hilink! :FennelStringEscape :Delimiter)

;; HTML
(hilink! :htmlTag :Delimiter)
(hilink! :htmlEndTag :Delimiter)

;; Clojure
(hilink! :clojureSymbol :Identifier)
(hilink! :clojureSpecial :Define)

;; Elixir
(hilink! :elixirId :Identifier)
(hilink! :elixirArguments :Identifier)
(hilink! :elixirListDelimiter :Delimiter)
(hilink! :elixirMapDelimiter :Delimiter)
(hilink! :elixirStructDelimiter :Delimiter)
(hilink! :elixirTupleDelimiter :Delimiter)
