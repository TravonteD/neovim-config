(require-macros :macros)

(set vim.opt_local.foldmethod :marker)
(set vim.opt_local.number false)

(local M {:filetype :vim
          :snippets {:func. "function <++>()\nendfunction"
                     :if. "if(<++>)\nendif"}})
(mcall :config :setup M)
M
