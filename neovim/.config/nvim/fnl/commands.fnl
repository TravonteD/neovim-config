(import-macros {: command!} :macros)

; General Commands
(command! :Q "q")
(command! :W "w")
(command! :Vterm "vsplit term://zsh")
(command! :Sterm "split term://zsh")
(command! :Econfigs "lua require('funcs').editconfigs()")
