(local M {:filetype "java"
          :snippets {:class. "public class <++> {\n}"
                     :func. "public static <++> <++>(<++>) {\n}"
                     :main. "public static void main(String []args) {\n  <++>\n}"
                     :print. "System.out.println(<++>);"
                     :for. "for (<++>) {\n}"
                     :if. "if (<++>) {\n}"
                     :import. "import <++>;"}})
(mcall :config :setup M)
M
