(import-macros {: mcall} :macros)
(local M {:filetype "vimwiki"})

(fn M.AutoHighLight []
  (let [language_declarations (vim.fn.systemlist (.. "rg '```(\\w+)' " (vim.fn.expand "%")))]
    (each [_ x (ipairs language_declarations)]
      (let [lang (x:match "%w+")]
        (vim.cmd (.. "PandocHighlight " lang))))))
(M.AutoHighLight)
(mcall :config :setup M)
M
