(command! :Runfile "lua ExecuteScript()" [:buffer])
(local M {:filetype "sh"
          :snippets {:if. "if [ <++> ] then;\nfi"
                     :case. "case $<++>\nesac"
                     :for. "for <++>\ndo\n  <++>\ndone"}})
(fn _G.ExecuteScript []
  (if (= (vim.fn.executable "%") 0)
    (vim.fn.system (.. "chmod +x" (vim.fn.shellescape (vim.fn.expand "%")))))
  (vim.cmd "split term://./%"))
(mcall :config :setup M)
M
