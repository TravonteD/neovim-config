(require-macros :macros)

(macro skeleton [ext tname]
  `(autocmd "BufNewFile" ,(string.format "*.%s" ext) ,(string.format ":-1read ~/.config/nvim/skeletons/%s" tname)))

(macro luafile [filetype ?filename]
  `(autocmd "Filetype" ,filetype ,(string.format "luafile ~/.config/nvim/lua/%s.lua" (if ?filename ?filename filetype))))

(augroup :General
         ; (autocmd "BufEnter" "*" "lua require'completion'.on_attach()")
         (autocmd "BufWritePre" "*" "s/\\s*$//e")
         (autocmd "WinEnter" "*" "set cursorline")
         (autocmd "WinLeave" "*" "set nocursorline")
         (autocmd "TermOpen" "*" "tnoremap <buffer> <Esc> <c-\\><c-n>")
         (autocmd "TermOpen" "*" "setlocal nonumber norelativenumber")
         (autocmd "BufWritePost" "*.vim" "source % | :e")
         (autocmd "BufNewFile,BufRead" "calcurse-note.*" "setlocal filetype=markdown")
         (autocmd "Filetype" "vimwiki" "set syntax=pandoc | lua require'vimwiki'.AutoHighLight()")
         (autocmd "TextYankPost" "*" "silent! lua vim.highlight.on_yank()"))

(augroup :Skeletons
         (skeleton :ly :lilypond)
         (skeleton :html :html)
         (skeleton :tex :latex)
         (skeleton :php :php)
         (skeleton :go :go)
         (skeleton "_spec.rb" :rspec))

(augroup :FennelLoad
         (luafile :elixir)
         (luafile :go)
         (luafile :vimwiki)
         (luafile :java)
         (luafile :html)
         (luafile "typescript,typescriptreact")
         (luafile :rust)
         (luafile :scss)
         (luafile :python)
         (luafile :lua)
         (luafile :tex)
         (luafile :markdown)
         (luafile :php)
         (luafile :crystal)
         (luafile :css)
         (luafile :fugitive)
         (luafile :ledger)
         (luafile :vim)
         (luafile :rspec)
         (luafile :ruby)
         (luafile :javascript)
         (luafile :clojure)
         (luafile :fennel :fnl))

(augroup :Lsp
         (autocmd "Filetype" "ruby,elixir" "lua require'funcs'.lsp_bindings()"))
