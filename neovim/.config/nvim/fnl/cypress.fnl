(local M {:filetype "cypress"
          :snippets {:context. "context('<++>', () => {\n  <++>\n})"
                     :beforeEach. "beforeEach(() => {\n  <++>\n}"
                     :describe. "describe('<++>', () => {\n  <++>\n})"
                     :it. "it('<++>', () => {\n  <++>\n})"
                     :expect. "expect(<++>).to.<++>"
                     :get_ "get('<++>')"
                     :visit. "cy.visit('<++>')"
                     :should_ "should('<++>')"}})
(mcall :config :setup M)
M
