(fn noremap [mode key seq opts]
  (local tab {:noremap true})
  (var buf? false)
  (local opts (or opts []))
  (each [_ opt (ipairs opts)]
    (if (= opt :buffer)
      (set buf? true)
      (tset tab opt true)))
  (if buf?
    `(vim.api.nvim_buf_set_keymap 0 ,mode ,key ,seq ,tab)
    `(vim.api.nvim_set_keymap ,mode ,key ,seq ,tab)))

(fn xnoremap [key seq opts]
  (noremap :x key seq opts))

(fn nnoremap [key seq opts]
  (noremap :n key seq opts))

(fn tnoremap [key seq opts]
  (noremap :t key seq opts))

(fn command! [name body opts]
  (local opts (or opts []))
  (var bar (icollect [_ opt (ipairs opts)] (.. "-" opt)))
  (local name (.. (table.concat bar " ") " " name))
  (let [cmd (string.format "command! %s %s" name body)]
    `(vim.cmd ,cmd)))

(fn cnoreabbrev [word replacement]
  (let [cmd (string.format "cnoreabbrev %s %s" word replacement)]
    `(vim.cmd ,cmd)))

(fn inoreabbrev [word replacement]
  (let [cmd (string.format "inoreabbrev %s %s" word replacement)]
    `(vim.cmd ,cmd)))

(fn highlight! [group args]
  (local formatted_args (icollect [k v (pairs args)]
                          (string.format "%s=%s" k v)))
  (local cmd (string.format "highlight! %s %s" group (table.concat formatted_args " ")))
  `(vim.cmd ,cmd))

(fn hilink! [group1 group2]
  (let [cmd (string.format "highlight! link %s %s" group1 group2)]
    `(vim.cmd ,cmd)))

(fn augroup [name ...]
  (local group (.. "augroup " name))
  `(do 
     (vim.cmd ,group)
     (vim.cmd "autocmd!") 
     (do 
       ,...)
     (vim.cmd "augroup END")))

(fn autocmd [group filelist args]
  (let [cmd (string.format "autocmd %s %s %s" 
                           group 
                           (if (= :table (type filelist))
                             (table.concat filelist ",") 
                             filelist) 
                           args)]
    `(vim.cmd ,cmd)))

(fn luafile [path]
  (let [cmd (string.format "luafile %s " path)]
    `(vim.cmd ,cmd)))

(fn safe_require [module]
  `(let [(ok?# msg#) (pcall require ,module)]
     (if (not ok?#)
       (error msg#))))

(fn with-lua [module ...]
  (let [[name mod] (if (sequence? module)
                     module 
                     [module module])]
    `(let [(ok?# ,(sym name)) (pcall require ,mod)]
       (when ok?#
         ,...))))

(fn mcall [module method args]
  `((. (require ,module) ,method) ,args))

{: augroup
 : autocmd
 : cnoreabbrev
 : command!
 : highlight!
 : hilink!
 : inoreabbrev
 : luafile
 : mcall
 : nnoremap
 : safe_require
 : tnoremap
 : xnoremap
 : with-lua}
