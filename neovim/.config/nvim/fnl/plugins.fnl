(import-macros {: with-lua
                : autocmd} :macros)
(vim.cmd "packadd cfilter")
(set vim.g.polyglot_disabled ["ruby" "rspec"])
(set vim.g.gutentags_cache_dir (vim.fn.expand "~/.cache/nvim/tags"))
(set vim.g.pandoc#syntax#conceal#urls 0)
(set vim.g.pandoc#syntax#codeblocks#embeds#use 1)
(set vim.g.completion_confirm_key "")
(set vim.g.completion_auto_change_source 1)
(set vim.g.conjure#filetype#fennel "conjure.client.fennel.stdio")

; Vim sexp
(set vim.g.sexp_filetypes (table.concat [:fennel :clojure :lisp :hy :scheme :lfe :janet] ","))

; Vimwiki
(set vim.g.vimwiki_list
     [{:path (or (os.getenv :VIMWIKI_PATH) "/home/tray/.local/wiki")
       :ext ".md"
       :syntax "markdown"
       :automatic_nested_syntaxes 1}])

(let [directory (.. (os.getenv :HOME) "/.local/share/nvim/site/pack/paqs/opt/paq-nvim")] 
  (when (= 0 (vim.fn.isdirectory directory))
    (vim.fn.system (string.format "git clone https://github.com/savq/paq-nvim.git %s" directory))
    (vim.cmd "packadd paq-nvim")
    (vim.cmd "PaqInstall")))

(vim.cmd "packadd paq-nvim")
(let [{: paq} (require :paq-nvim)]
  (paq {1 "savq/paq-nvim" :opt true})
  (paq "norcalli/typeracer.nvim")
  (paq "ledger/vim-ledger")
  (paq "hkupty/iron.nvim")
  (paq "junegunn/vader.vim")
  (paq "vimwiki/vimwiki")
  (paq "tpope/vim-fugitive")
  (paq "sheerun/vim-polyglot")
  (paq "norcalli/nvim-colorizer.lua")
  (paq "travonted/luajob")
  (paq "travonted/luapack")
  (paq "machakann/vim-sandwich")
  (paq "neovim/nvim-lsp")
  (paq "chriskempson/base16-vim")
  (paq "tomtom/tcomment_vim")
  (paq "ludovicchabant/vim-gutentags")
  (paq "vim-pandoc/vim-pandoc-syntax")
  (paq "haorenW1025/completion-nvim")
  (paq "junegunn/fzf")
  (paq "nvim-lua/popup.nvim")
  (paq "nvim-lua/plenary.nvim")
  (paq "nvim-telescope/telescope.nvim")
  (paq "nvim-telescope/telescope-fzy-native.nvim")
  (paq "mbbill/undotree")
  (paq "nvim-treesitter/nvim-treesitter")
  (paq "theprimeagen/vim-be-good")
  (paq "bakpakin/fennel.vim")
  (paq "guns/vim-sexp")
  (paq "Olical/conjure")
  (paq "Olical/aniseed")
  (paq "hylang/vim-hy")
  (paq "lfe-support/vim-lfe")
  (paq "justinmk/vim-dirvish")
  (paq "janet-lang/janet.vim")
  (paq "TimUntersberger/neogit"))

(vim.cmd "packloadall")

(with-lua :lspconfig
  (lspconfig.sumneko_lua.setup 
    {:settings {:Lua {:diagnostics {:globals ["vim"]}}}})
  (lspconfig.elixirls.setup {:cmd ["/Users/traydennis/Downloads/elixir-ls/language_server.sh"]})
  (lspconfig.pyls.setup {})
  (lspconfig.intelephense.setup {})
  (lspconfig.tsserver.setup {})
  (lspconfig.cssls.setup {})
  (lspconfig.solargraph.setup 
    {:settings {:solargraph {:diagnostics true}}}))

(autocmd :BufEnter [:ruby :lua :python :elixir :markdown :fennel :clojure] "lua require('completion').on_attach()")

(with-lua :telescope
  (local actions (require :telescope.actions))
  (telescope.setup {:defaults {:mappings {:i {"<c-g>" actions.close}}}})
  (telescope.load_extension :fzy_native))

(with-lua :iron
  (iron.core.set_config {:repl_open_cmd "split"})        
  (iron.core.add_repl_definitions
    {:lfe {:lfe {:command ["rebar3" "lfe" "repl"]}}
     :janet {:janet {:command ["janet"]}}
     :elixir {:mix {:command ["iex" "-S" "mix" "phx.server"]}}}))
