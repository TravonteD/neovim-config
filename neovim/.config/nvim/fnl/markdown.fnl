(require-macros :macros)

(set vim.opt_local.spell true)
(set vim.opt_local.tabstop 2)
(set vim.opt_local.shiftwidth 2)
(set vim.opt_local.expandtab true)
(set vim.opt_local.linebreak true)
(set vim.opt_local.makeprg "pandoc % -o %:r.pdf")
(set vim.opt_local.syntax :pandoc)

(nnoremap "<leader>s" ":setlocal spell!<cr>" [:buffer])
(nnoremap "<leader>z" "1z=" [:buffer])

(command! :OpenPreview "make <bar> call jobstart('zathura '.expand('%:r').'.pdf')" [:buffer])

(local M {:filetype "markdown"
          :snippets {:link. "[<++>](<++>)"}})
(mcall :config :setup M)
M
