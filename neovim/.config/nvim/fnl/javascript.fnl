(require-macros :macros)

(set vim.opt_local.tabstop 2)
(set vim.opt_local.shiftwidth 2)
(set vim.opt_local.softtabstop 0)

(set vim.opt_local.define "^\\s*function\\|^\\s*\\(var\\|let\\|const\\)\\s*\\ze\\i\\+\\s*=*\\|^\\s*\\ze\\i\\+\\s*[:]\\s*(*function\\s*")

(command! :RunFile ":sp term://node %" [:buffer])

(when (string.find (vim.fn.expand "%") "spec")
  (command! :CyStart "lua Cypress('start')" [:buffer])
  (command! :CyClose "lua Cypress('stop')" [:buffer])
  (fn _G.Cypress [cmd]
    (match cmd
      "start" (set vim.g.cypress_job (vim.fn.jobstart "npx cypress open"))
      "stop" (vim.fn.jobstop vim.g.cypress_job)))
  (vim.cmd "luafile ~/.config/nvim/lua/cypress.lua"))

(local M {:javascript "javascript"
          :snippets {:for. "for (<++>) {\n}"
                     :forEach_ "foreach((<++>) => {\n});"
                     :func. "function <++>(<++>) {\n}"
                     :if. "if (<++>) {\n}"
                     :import. "import <++> from '<++>';"
                     :log. "console.log(<++>);"
                     :require. "const <++> = require('<++>');"
                     :switch. "switch (<++>) {\n}"
                     :try. "try {\n<++>\n} catch (<++>) {\n}"
                     :while. "while (<++>) {\n}"}})
(mcall :config :setup M)
M
