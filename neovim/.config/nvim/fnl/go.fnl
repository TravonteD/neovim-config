(require-macros :macros)

(nnoremap "<leader>r" ":GoRun<cr>" [:silent :buffer])
(let [snippets {:for. "for <++> {\n}"
                :func. "func <++>(<++>) <++> {\n }"
                :if. "if <++> {\n}"
                :import. "import (\n  <++>\n)"
                :interface. "type <++> interface {\n}"
                :loop. "for {\n  <++>\n}"
                :print. "fmt.Println(<++>)"
                :struct. "type <++> struct {\n}"
                :switch. "switch <++> {\n}"
                :while. "for <++> {\n}"}
      M {:filetype "go"
         :snippets snippets}]
  ((. (require "config") "setup") M)
  M)

