(require-macros :macros)

(set vim.opt_local.tabstop 4)
(set vim.opt_local.shiftwidth 4)
(set vim.opt_local.foldmethod :indent)
(command! :Runfile ":sp term://php %")

(local M {:filetype :php
          :snippets {:func. "function <++>(<++>) {\n}"
                     :if. "if (<++>) {\n}"
                     :else. "} else {\n<++>"
                     :for. "for (<++>) {\n}"
                     :foreach. "foreach (<++> as <++>) {\n}"
                     :phpfunc. "<?php\nfunction <++>(<++>) {\n?>\n<++>\n<?php } ?>"
                     :phpif. "<?php if (<++>) { ?>\n<?php } ?>"
                     :phpelse. "<? } else { ?>\n<++>"
                     :phpforeach. "<?php foreach (<++> as <++>) { ?>\n<?php } ?>"
                     :phpwhile. "<?php while (<++>) { ?>\n<?php } ?>"
                     :php. "<?php\n<++>\n?>"
                     :echo. "<?= $<++> ?>"}})
(mcall :config :setup M)
M
