(require-macros :macros)

(set vim.opt_local.foldmethod :indent)
(set vim.opt_local.number true)

(command! :RunFile ":sp term://python %" [:buffer])
(command! :Repl ":sp term://python <bar> startinsert" [:buffer])

(when (vim.fn.executable "pyls")
  (set vim.bo.omnifunc "v:lua.vim.lsp.omnifunc"))

(local M {:filetype "python"
          :snippets {:def. "def <++>(<++>):"
                     :class. "class <++>:"
                     :if. "if <++>:"
                     :for. "for <++> in <++>:"
                     :while. "while <++>:"}})
(mcall :config :setup M)
M
