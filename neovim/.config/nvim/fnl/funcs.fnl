(require-macros :macros)
(fn eatchar [pat]
  (let [c (vim.fn.nr2char (vim.fn.getchar 0))]
    (if (c:find pat) "" c)))

(fn editconfigs []
  (let [{: find_files} (require "telescope.builtin")]
    (find_files
      {:prompt_title "Neovim Configs"
       :cwd "~/.config/nvim"})))

(fn fold_text []
  (let [line (vim.fn.getline vim.v.foldstart)
        sub (line:gsub "{{{" "")
        numlines (- vim.v.foldend vim.v.foldstart)]
    (.. sub " " numlines " lines ")))

(fn snippet [ft name]
  (let [snippets (. (require ft) "snippets")]
    (string.gsub (. snippets name) "\n$" "")))

(fn join_lines []
  (vim.cmd "normal! J")
  (let [line (vim.fn.split (vim.fn.getline ".") "\\zs")
        char (. line (. (vim.fn.getpos ".") 3))]
    (when (= char " ")
      (vim.cmd "normal! x"))))

(fn make_var_extract [label]
  (when (not= label "")
    (local label (.. label " ")))
  (fn []
    (let [name (vim.fn.input "varname: ")
          commands "normal! gv\"vy
          execute 'normal! gvc%s'
          execute 'normal! O%s%s = '
          normal! \"vp"]
      (vim.api.nvim_exec (commands:format name label name) false))))

(fn ijump []
  (let [word (vim.fn.expand "<cword>")
        choice (vim.fn.input "select reference: ")]
    (do (->> word
             (string.format "ilist  /%s")
             (vim.cmd))
      (->> word
           (string.format "ijump %s /%s" choice)
           (vim.cmd)))))

(fn lsp_bindings []
  (nnoremap "gD" "<cmd>lua vim.lsp.buf.definition()<cr>" [:buffer :silent])
  (nnoremap "K" "<cmd>lua vim.lsp.buf.hover()<cr>" [:buffer :silent])
  (nnoremap "<leader>K" "<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<cr>" [:buffer :silent])
  (nnoremap "gr" "<cmd>lua vim.lsp.buf.references()<cr>" [:buffer :silent])
  (set vim.opt_local.omnifunc "v:lua.vim.lsp.omnifunc"))

{: eatchar
 : editconfigs
 : fold_text
 : ijump
 : join_lines
 : lsp_bindings
 : make_var_extract
 : snippet}
