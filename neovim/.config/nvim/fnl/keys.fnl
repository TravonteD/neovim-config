(require-macros :macros)

(nnoremap "Y" "y$")
(xnoremap "<leader>y" "\"+y")
(nnoremap "<leader>y" "\"+y")
(xnoremap "<leader>p" "\"+p")
(nnoremap "<leader>p" "\"+p")
(nnoremap "<leader>o" "o<Esc>")
(nnoremap "<leader><Space> " "/<++><CR>c4l")
(nnoremap "<leader>e" ":20Lexplore<CR>")
(nnoremap "<leader>N" ":bprevious<CR>")
(nnoremap "<leader>n" ":bnext<CR>")
(nnoremap "<leader>r" ":r!")
(nnoremap "<Left>" ":bprevious<CR>")
(nnoremap "<Right>" ":bnext<CR>")
(nnoremap "<Down>" ":cnext<cr>")
(nnoremap "<Up>" ":cprevious<cr>")
(nnoremap "<C-h>" "<C-w>h")
(nnoremap "<C-j>" "<C-w>j")
(nnoremap "<C-k>" "<C-w>k")
(nnoremap "<C-l>" "<C-w>l")
(tnoremap "<C-h>" "<C-\\><C-n><C-w>h")
(tnoremap "<C-j>" "<C-\\><C-n><C-w>j")
(tnoremap "<C-k>" "<C-\\><C-n><C-w>k")
(tnoremap "<C-l>" "<C-\\><C-n><C-w>l")
(nnoremap "R," "\"ryiw:s/<c-r>r/")
(nnoremap "<leader>c" ":TComment<cr>")
(xnoremap "<leader>c" ":TComment<cr>")
(nnoremap "<leader>gs" ":G<cr>")
(nnoremap "<leader>gc" ":G commit<cr>")
(nnoremap "<C-p>" ":Telescope find_files<cr>")
(nnoremap "<C-g>" ":Telescope git_files<cr>")
(nnoremap "<leader>gl" ":lua logs()<cr>")
(nnoremap "<leader>d" ":dlist /<cr>:djump 1 /")
(nnoremap "<leader>b" ":Telescope buffers<cr>")
(nnoremap "<leader>j" ":lua require'funcs'.join_lines()<cr>")
(nnoremap "<leader>I" ":lua require'funcs'.ijump()<cr>")
(nnoremap "<leader>D" ":ijump /<c-r>=expand('<cword>')<cr><cr>")

; Abbreviations
(cnoreabbrev "src." "source $MYVIMRC")
(cnoreabbrev "erc" "e $MYVIMRC")
(cnoreabbrev "eft" ":lua require'funcs'.open_file_type('e', false)")
(inoreabbrev "teh" "the")
(inoreabbrev "recieve" "receive")
(inoreabbrev "treu" "true")
(inoreabbrev "taht" "that")
