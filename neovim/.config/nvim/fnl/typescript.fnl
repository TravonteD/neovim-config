(local M {:filetype "typescript"
          :snippets {:func. "function <++>(<++>) {\n}"
                     :if. "if (<++>) {\n}"
                     :switch. "switch (<++>) {\n}"
                     :for. "for (<++>) {\n}"
                     :while. "while (<++>) {\n}"
                     :forEach_ "foreach((<++>) => {\n});"
                     :log. "console.log(<++>);"
                     :try. "try {\n<++>\n} catch (<++>) {\n}"
                     :require. "const <++> = require('<++>');"
                     :import. "import <++> from '<++>';"
                     :interface. "interface <++> {\n}"
                     :class. "class <++> {\n}"}})
(mcall :config :setup M)
M
