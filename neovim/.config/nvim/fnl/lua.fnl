(set vim.opt_local.makeprg "luacheck --formatter plain %")
(set vim.opt_local.define "function")

(command! :Extract ":lua LuaExtract()" [:buffer])

(xnoremap "<leader>v" ":<C-u>Extract<cr>" [:buffer])

(local M {:filetype "lua"
          :snippets {:func. "function <++>(<++>)\nend"
                     :if. "if <++> then\nend"
                     :for. "for <++> in <++> do\nend"
                     :command. "api.nvim_command(<++>)"
                     :exec. "api.nvim_exec(<++>)"
                     :inspect. "print(vim.inspect(<++>))"}})

(global LuaExtract (mcall :funcs :make_var_extract "local"))
(mcall :config :setup M)
M
