(require-macros :macros)

(set vim.opt_local.makeprg "pdflatex %")
(nnoremap "<leader>r" "<cmd>make<cr>" [:buffer])
(command! :OpenPreview "call jobstart('zathura '.expand('%:r').'.pdf')")

(local M {:filetype "tex"
          :snippets {}})
(mcall :config :setup M)
M
