(require-macros :macros)

(set vim.opt_local.shiftwidth 4)
(set vim.opt_local.foldlevel 100)
(set vim.opt_local.foldmethod :expr)
(set vim.opt_local.foldexpr "v:lua.SassFolding(v:lnum)")

(set vim.opt_local.includeexpr "")
(set vim.opt_local.define "^\\s*\\$\\ze\\i\\w*|^\\s*@mixin")

(local M {:filetype "scss"
          :snippets {:media. "@media(<++>) {\n}"
                     :import. "@import '<++>';"
                     :include. "@include '<++>';"
                     :error. "@error '<++>';"
                     :mixin. "@mixin <++> {\n}"
                     :if. "@if <++> {\n}"}})

(fn _G.SassFolding [num]
  (let [line (vim.fn.getline num)]
    (if (line:find "{$")
      "a1"
      (if (line:find "}$")
        "s1"
        "="))))
(mcall :config :setup M)
M
