FILES=$(subst fnl,lua,$(shell ls neovim/.config/nvim/fnl/*.fnl | grep -v macro))

.PHONY: all
all: neovim/.config/nvim/init.lua neovim/.config/nvim/lua/fnl.lua  $(FILES)

neovim/.config/nvim/init.lua: neovim/.config/nvim/init.fnl
	cd $(<D) && fennel --compile $(<F) > $(@F)

neovim/.config/nvim/lua/%.lua: neovim/.config/nvim/fnl/%.fnl neovim/.config/nvim/fnl/macros.fnl
	mkdir -p neovim/.config/nvim/lua
	cd $(<D) && fennel --compile $(<F) > ../lua/$(@F)

neovim/.config/nvim/fnl/%.fnl: neovim/.config/nvim/lua/%.lua .FORCE

.PHONY: .FORCE

.PHONY: install
install:
	stow -t ~ neovim

.PHONY: uninstall
uninstall:
	stow -D neovim
